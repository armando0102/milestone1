﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour {
	// Update is called once per frame
	void Update () {
        //sets the speed to prefered int
        var speed = 2;
        //causes forward movement without stop
        transform.position += transform.up * Time.deltaTime * speed;


	}
}
